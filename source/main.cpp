#include "windows.h"

void MyFunction(void)
{
    int Five = 5;
    int Three = 3;
    int Result = Five + Three;
}

int AddTwoNumbers(int Number1, int Number2)
{
    int Result = Number1 + Number2;
    return Result;
}

void PrintNumber(int Number)
{
    // Wir wissen noch nicht, wie viele Stellen unsere
    // Zahl haben wird, für den moment gehen wir einfach
    // einmal davon aus, das es nicht mehr als 20 werden
    char TextBuffer[20];

    // Wir müssen uns merken, an welcher Position im Array
    // wir aktuell sind
    int CurrentIndex = 0;

    while (Number > 0)
    {
        int ToPrint = Number % 10;
        TextBuffer[CurrentIndex] = ToPrint + 48;
        CurrentIndex++;
        Number = Number / 10;
    }

    // Am Ende eines C-Strings gehört immer eine 0
    TextBuffer[CurrentIndex] = 0;

    // Jetzt nochmal alles umdrehen
    // Das Ende des Strings soll auch das Ende bleiben
    CurrentIndex--;
    int RunningIndex = 0;

    // Wir haben nun zwei Indizes
    // RunningIndex - Läuft von links nach rechts
    // CurrentIndex - Läuft von rechts nach link

    // Solange die beiden Indizes noch nicht aneinander
    // vorbei gelaufen sind, gibt es noch stellen, die
    //  getauscht werden müssen
    while (RunningIndex < CurrentIndex)
    {
        // Wir müssen uns Temporär einen der zu
        // tauschenden Werte merken, da dieser überschrieben wird
        char Temp = TextBuffer[RunningIndex];

        // Die beiden Werte innerhalb des Arrays tauschen
        TextBuffer[RunningIndex] = TextBuffer[CurrentIndex];
        TextBuffer[CurrentIndex] = Temp;

        // Die Indizes in die jeweilige Richtung laufen lassen
        RunningIndex++;
        CurrentIndex--;
    }
    OutputDebugStringA(TextBuffer);
}

void main(void)
{
    PrintNumber(6234);
    char *Text = "Hello\n";
    OutputDebugStringA(Text);
    char TextBuffer[2];
    TextBuffer[0] = '5';
    TextBuffer[1] = 0;
    OutputDebugStringA(TextBuffer);

    int Number1 = 4;
    int Number2 = 7;
    MyFunction();
    int Number1PlusNumber2 = AddTwoNumbers(Number1, Number2);
}
